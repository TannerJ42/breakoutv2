﻿using UnityEngine;
using System.Collections;

public class SwordController : MonoBehaviour 
{
	public GameObject leftBorder;
	public GameObject rightBorder;

	// Use this for initialization
	void Start () 
	{
	
	}

	void FixedUpdate()
	{
		Vector3 position = transform.position;

		var pos = Input.mousePosition;
		pos = Camera.main.ScreenToWorldPoint(pos);

		position.x = pos.x;

		if (position.x < leftBorder.transform.position.x)
			position.x = leftBorder.transform.position.x;
		if (position.x > rightBorder.transform.position.x)
			position.x = rightBorder.transform.position.x;

		transform.position = position;
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
}
