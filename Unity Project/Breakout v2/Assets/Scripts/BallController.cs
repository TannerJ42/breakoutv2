﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {
	
	public float ballInitialVelocity = 600f;
	
	
	private Rigidbody2D rb;
	private bool ballInPlay;
	
	void Awake () 
	{
		
		rb = GetComponent<Rigidbody2D>();
		
	}

	void Start ()
	{
		transform.parent = null;
		ballInPlay = true;
		rb.isKinematic = false;
		rb.AddForce(new Vector3(ballInitialVelocity, ballInitialVelocity, 0));
	}

	void Update () 
	{

	}
}