﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour 
{
	Animator anim;
	bool breaking = false;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	// call this when the block is done being destroyed.
	public void OnDestroy()
	{
		Destroy (this.gameObject, 1f);
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject.tag != "ball" ||
		    breaking) 
		{
			return;
		}
		anim.SetTrigger("break");
		breaking = true;
		var colliders = GetComponentsInChildren<Collider2D>();

		foreach (Collider2D co in colliders)
		{
			co.enabled = false;
		}
	}
}
